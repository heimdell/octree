
import Data.List (isSubsequenceOf, sort)

import OSP

import Test.QuickCheck
import Test.Hspec

inVicinity' :: HasAABB a => Pt -> [a] -> [a]
inVicinity' pt = filter ((aabbOfPt pt `inside`) . aabb)

newtype Coord = Coord { unCoord :: Float }
newtype Span  = Span  { unSpan  :: Float }

instance Arbitrary Coord where
  arbitrary = do
    Coord <$> elements [0.. 1000]

instance Arbitrary Span where
  arbitrary = do
    Span <$> elements [0.. 20]

instance Arbitrary Pt where
  arbitrary = pure Pt
    <*> coord
    <*> coord
    <*> coord
    where
      coord = unCoord <$> arbitrary

instance Arbitrary AABB where
  arbitrary = pure AABB
    <*> coord
    <*> coord
    <*> coord
    <*> spanning
    <*> spanning
    <*> spanning
    where
      coord    = unCoord <$> arbitrary
      spanning = unSpan  <$> arbitrary

main :: IO ()
main = do
  let
    makeTree = fromList Conditions
      { maxObjectsPerNode = 8
      , depthLimit        = 20
      , volume            = cubeOfEdge 1000 origin
      }

  hspec do
    describe "works the same way list of object does" do
      it "has no false negative in `inVicinity`" do
        quickCheck \(objects :: [AABB]) pt -> do
          let
            expected = inVicinity' pt objects
            got      = inVicinity  pt (makeTree objects)

          sort expected `isSubsequenceOf` sort got

      it "does not hang when more that N objects have the same coords" do
        let
          points  = replicate 100 (aabbOfPt origin)
          points' = inVicinity origin (makeTree points)

        sort points == sort points'

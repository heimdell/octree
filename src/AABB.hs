
module AABB where

import Control.DeepSeq
import GHC.Generics (Generic)

-- | Axis-aligned bounding box.
--
data AABB = AABB
  { x, y, z, w, l, h :: Float
  }
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass (NFData)

-- | Create cubical bounding box from the point.
--
cubeOfEdge :: Float -> Pt -> AABB
cubeOfEdge e (Pt x y z) = AABB x y z e e e

-- | Check if one AABB is completely inside another one.
inside :: AABB -> AABB -> Bool
AABB x y z w l h `inside` AABB x' y' z' w' l' h'
  =  x >= x' && x + w <= x' + w'
  && y >= y' && y + l <= y' + l'
  && z >= z' && z + h <= z' + h'

-- | Get AABB from object.
--
class HasAABB a where
  aabb :: a -> AABB

-- | Trivial instance for testing.
--
instance HasAABB AABB where
  aabb = id

split :: AABB -> [AABB]
split (AABB x y z w l h) =
  [ AABB x  y  z  w2 l2 h2
  , AABB x' y  z  w2 l2 h2
  , AABB x  y' z  w2 l2 h2
  , AABB x' y' z  w2 l2 h2
  , AABB x  y  z' w2 l2 h2
  , AABB x' y  z' w2 l2 h2
  , AABB x  y' z' w2 l2 h2
  , AABB x' y' z' w2 l2 h2
  ]
  where
    x' = x + w2
    y' = y + l2
    z' = z + h2
    w2 = w / 2
    l2 = l / 2
    h2 = h / 2

grow :: AABB -> AABB
grow (AABB x y z w l h) = (AABB x y z (w * 2) (l * 2) (h * 2))

-- | A point in space.
--
data Pt = Pt Float Float Float
  deriving stock (Eq, Show)

-- | The point (0, 0, 0)
--
origin :: Pt
origin = Pt 0 0 0

-- | Zero-sized AABB of a point.
ofPt :: Pt -> AABB
ofPt (Pt x y z) = AABB x y z 0 0 0

directionTo :: Pt -> AABB -> (Int, AABB)
directionTo pt cube =
  let children = split cube
  in head (filter ((ofPt pt `inside`) . snd) (zip [0..] children))
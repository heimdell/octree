
{-# OPTIONS -Wno-orphans #-}

module BlockTree where

import Control.Applicative
import Control.Monad.Primitive
import Control.Monad.State.Strict
import Control.Monad.Except

import Data.Bits
import Data.Default
import Data.List.NonEmpty as NE
import qualified Data.Vector.Unboxed.Mutable as UM
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Mutable as M
import qualified Data.Vector as V
import System.IO.Unsafe
import Unsafe.Coerce

import qualified AABB

instance (Show a, U.Unbox a) => Show (UM.MVector s a) where
  show v =
    unsafePerformIO do
      v' <- U.freeze (unsafeCoerce v :: UM.MVector (PrimState IO) a)
      return (show v')

instance Show a => Show (M.MVector s a) where
  show v =
    unsafePerformIO do
      v' <- V.freeze (unsafeCoerce v :: M.MVector (PrimState IO) a)
      return (show v')

class PrimMonad m => HasVar m where
  type Var m :: * -> *
  new      :: a -> m (Var m a)
  retrieve :: Var m a -> m a
  ($=)     :: Var m a -> a -> m ()

instance (HasVar m, MonadTrans t, PrimMonad (t m)) => HasVar (t m) where
  type Var (t m) = Var m
  new      = lift . new
  retrieve = lift . retrieve
  v $= a   = lift $ v $= a

data Octree col m mat = Octree
  { space  :: BlockTree col m mat
  , volume :: AABB.AABB
  , scale  :: Int
  }

deriving stock instance (Show (Var m col), U.Unbox mat, Show mat, Show col) => Show (Octree col m mat)

data BlockTree col m mat
  = Block     (Var m col) (UM.MVector (PrimState m)  mat)
  | Monolith   col         mat
  | Joint     (Var m col) (M.MVector  (PrimState m) (BlockTree col m mat))

deriving stock instance (Show (Var m col), U.Unbox mat, Show mat, Show col) => Show (BlockTree col m mat)

type Prim st m = (PrimMonad m, PrimState m ~ st)

filled :: (UM.Unbox mat, HasColor mat col) => mat -> BlockTree col m mat
filled mat = Monolith (color mat) mat

type Zipper      col m mat n = ExceptT () (StateT (ZipperState col m mat) n)
type ZipperState col m mat   = NE.NonEmpty (Layer col m mat)

data Layer col m mat
  = Layer
    { replaced :: Bool
    , back     :: Int
    , locus    :: Octree col m mat
    }
  | Cell
    { x     :: Int
    , y     :: Int
    , z     :: Int
    , block :: UM.MVector (PrimState m) mat
    , col   :: Var m col
    , chunk :: AABB.AABB
    }

deriving stock instance (Show (Var m col), U.Unbox mat, Show mat, Show col) => Show (Layer col m mat)

class Additive col where
  add          :: col -> col -> col
  subs         :: col -> col -> col
  one16th      :: col -> col
  one16cubeth  :: col -> col
  the15of16col :: col -> col

class Additive col => HasColor mat col | mat -> col where
  color :: mat -> col

treeColor :: (HasVar n, Var m ~ Var n) => BlockTree col m mat -> n col
treeColor = \case
  Block    col _ -> retrieve col
  Monolith col _ -> return   col
  Joint    col _ -> retrieve col

type CanZip col mat st n m =
  ( Prim st m
  , Prim st n
  , Var n col ~ Var m col
  , HasVar n
  , UM.Unbox mat
  , HasColor mat col
  , Default mat
  )

goto
  :: CanZip col mat st n m
  => AABB.Pt
  -> Zipper col m mat n ()
goto pt = do
  goUpUntilFits pt
  goDownTo pt

goUpUntilFits
  :: CanZip col mat st n m
  => AABB.Pt
  -> Zipper col m mat n ()
goUpUntilFits pt = go
  where
    go = do
      get >>= \case
        r@(layer@Layer{} :| []) ->
          case locus layer of
            Octree space vol scale -> do
              unless (AABB.ofPt pt `AABB.inside` vol) do
                vec    <- M.replicate 8 (filled def)
                col    <- treeColor space
                colVar <- new (one16th col)
                M.unsafeWrite vec 0 space
                put
                  ( Layer True 0
                      (Octree (Joint colVar vec) (AABB.grow vol) (scale + 1))
                  `NE.cons` r
                  )
                go
        layer@Layer{} :| _  -> do
          case locus layer of
            Octree _ vol _ -> do
              unless (AABB.ofPt pt `AABB.inside` vol) do
                up
                go

        Cell{chunk} :| _ -> do
          unless (AABB.ofPt pt `AABB.inside` chunk) do
            up
            go

goDownTo
  :: CanZip col mat st n m
  => AABB.Pt
  -> Zipper col m mat n ()
goDownTo pt = go
  where
    go = do
        inDirectionOf pt
        go
      <|>
        return ()

up
  :: CanZip col mat st n m
  => Zipper col m mat n ()
up = do
  get >>= \case
    Layer replaced i loc
      :| (whole@(locus -> Octree (Joint colVar branches) _ _) : r) -> do
      when replaced do
        oldCol <- retrieve colVar
        newCol <- treeColor (space loc)
        colVar $= (one16th newCol `add` the15of16col oldCol)
        M.unsafeWrite branches i (space loc)
        put (whole :| r)

    _ :| r : p ->
      put (r :| p)

    _ :|  [] ->
      throwError ()

inDirectionOf
  :: CanZip col mat st n m
  => AABB.Pt
  -> Zipper col m mat n ()
inDirectionOf pt = do
  get >>= \case
    layer@Layer{} :| r -> do
      case locus layer of
        Octree (Block colVar vec) vol _ -> do
          let (x, y, z) = ptMod16 pt
          put (Cell x y z vec colVar vol :| layer : r)

        Octree (Monolith col mat) vol scale -> do
          colVar <- new col
          replacement <-
            if scale == 0
            then do
              matrix <- UM.replicate (16 * 16 * 16) mat
              return $ Block colVar matrix
            else do
              branches <- M.replicate 8 (Monolith col mat)
              return $ Joint colVar branches

          put (Layer True (back layer) (Octree replacement vol scale) :| r)
          inDirectionOf pt

        Octree (Joint _ branches) vol scale -> do
          let (i, sub) = AABB.directionTo pt vol
          branch <- M.unsafeRead branches i
          put (Layer False i (Octree branch sub (scale - 1)) :| layer : r)

    cell :| r -> do
      let (x, y, z) = ptMod16 pt
      put $ cell { x = x, y, z } :| r

ptMod16 :: AABB.Pt -> (Int, Int, Int)
ptMod16 (AABB.Pt x y z) = (round x .&. 15, round y .&. 15, round z .&. 15)

## Octal space partitioning

### Why

You have 1k-10k objects in space and you want to see if any of then intersect with a ray.
Also, you don't want to check everyone of them.

Here, this package comes into play: you put the objects into an `OSP` and you can run queries ot it which
will return a lot less false positives for you to sort out than brute-force search.

### Details

The object type you put in should implement `HasAABB` typeclass - basically, they should be able to calculate their
own axis-aligned bounding box.

You can put the objects in all at once or one-by-one.

import OSP

import Criterion.Main
import Test.QuickCheck

newtype Coord = Coord { unCoord :: Float }
newtype Span  = Span  { unSpan  :: Float }

instance Arbitrary Coord where
  arbitrary = do
    Coord <$> elements [0.. 1000]

instance Arbitrary Span where
  arbitrary = do
    Span <$> elements [0.. 20]

instance Arbitrary Pt where
  arbitrary = pure Pt
    <*> coord
    <*> coord
    <*> coord
    where
      coord = unCoord <$> arbitrary

instance Arbitrary AABB where
  arbitrary = pure AABB
    <*> coord
    <*> coord
    <*> coord
    <*> spanning
    <*> spanning
    <*> spanning
    where
      coord    = unCoord <$> arbitrary
      spanning = unSpan  <$> arbitrary

makeTree :: [AABB] -> OSP AABB
makeTree = fromList Conditions
  { maxObjectsPerNode = 8
  , depthLimit        = 20
  , volume            = cubeOfEdge 1000 origin
  }

main :: IO ()
main = defaultMain
  [
    env (makeTree <$> generate (vectorOf 1000 arbitrary)) \tree' ->
      bench "1k objects" do
        flip nfAppIO tree' \tree -> do
          pt <- generate arbitrary
          return (length (inVicinity pt tree))
  , env (makeTree <$> generate (vectorOf 10000 arbitrary)) \tree' ->
      bench "10k objects" do
        flip nfAppIO tree' \tree -> do
          pt <- generate arbitrary
          return (length (inVicinity pt tree))

  , env (makeTree <$> generate (vectorOf 100000 arbitrary)) \tree' ->
      bench "100k objects" do
        flip nfAppIO tree' \tree -> do
          pt <- generate arbitrary
          return (length (inVicinity pt tree))

  , env (makeTree <$> generate (vectorOf 1000000 arbitrary)) \tree' ->
      bench "1M objects" do
        flip nfAppIO tree' \tree -> do
          pt <- generate arbitrary
          return (length (inVicinity pt tree))
  ]
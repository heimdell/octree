
{-|
Module      : OSP
Description : Octal space partition
Copyright   : (c) Kirill Andreev, 2019
License     : WTFPL
Maintainer  : hindmost.one@gmail.com
Stability   : experimental
Portability : POSIX

Octal space partition

Can be used to determine what subset of objects is near some point in
space.

Internally, its an octree that stores sets (lists) of objects inside
both leaves and branches, and vicinity check collects objects along
the spine.

It autosplits node on insert, if it is populated by more than
`maxObjectsPerNode` objects.

To prevent infinitely deep trees coming from N objects in the exact
same place, there is a `depthLimit` setting.

The tree works only with the cube of space it was created for and will lie
about objects and points not completely inside the cube.
-}

module OSP
  (
    -- * The tree
    OSP
  , Conditions (..)
  , empty
  , fromList
  , insert
  , inVicinity

    -- * Axis-aligned bounding box
  , HasAABB (..)
  , AABB (..)
  , cubeOfEdge
  , inside
  )
  where

import Control.DeepSeq
import Data.Vector (Vector, (!), (//))
import qualified Data.Vector as Vector
import GHC.Generics (Generic)

import AABB

-- | Space separation tree.
--
data OSP a = OSP
  { partition  :: Octree a
  , conditions :: Conditions
  }
  deriving stock (Eq, Show, Generic)
  deriving anyclass (NFData)

-- | Presets.
--
data Conditions = Conditions
  { maxObjectsPerNode :: Int   -- ^ The amount of objects causing node resplit.
  , depthLimit        :: Int   -- ^ The maximum tree depth.
  , volume            :: AABB  -- ^ The (cubic) volume of space the tree covers.
  }
  deriving stock (Eq, Show, Generic)
  deriving anyclass (NFData)

data Octree a
  = Branch [a] (Vector (Octree a))
  | Leaf   [a] Int
  deriving stock (Eq, Show, Generic)
  deriving anyclass (NFData)

-- | Create the tree with no objects.
--
empty :: Conditions -> OSP a
empty = OSP emptyTree

-- | Create the tree with some objects.
--
--  @foldr insert . empty@
--
fromList :: HasAABB a => Conditions -> [a] -> OSP a
fromList = foldr insert . empty

emptyTree :: Octree a
emptyTree = Leaf [] 0

crushedTree :: Octree a
crushedTree = Branch [] (Vector.replicate 8 emptyTree)

-- | Add an object into an `OSP`.
--
insert :: HasAABB a => a -> OSP a -> OSP a
insert thing osp@OSP { partition, conditions } =
    osp
      { partition =
          go
            (volume conditions)
            (depthLimit conditions)
            thing
            partition
      }
  where
    go box limit object = \case
      Branch items children ->
        case direction (aabb object) box of
          Nothing -> Branch (object : items) children
          Just (i, subBox) ->
            Branch items $ children /% (i, go subBox (limit - 1) object)

      Leaf items count -> do
        if count >= maxObjectsPerNode conditions && limit > 0
        then
          foldr (go box limit) crushedTree (object : items)
        else
          Leaf (object : items) (count + 1)

direction :: AABB -> AABB -> Maybe (Int, AABB)
direction target bigBox =
    let
      tries     = zip [0..] $ split bigBox
      fitsChild = filter ((target `inside`) . snd) tries
    in
      case fitsChild of
        []    -> Nothing
        a : _ -> Just a

-- | Get all objects in vicinity the point.
--
inVicinity :: Pt -> OSP a -> [a]
inVicinity pt osp = go (volume (conditions osp)) (partition osp)
  where
    go box = \case
      Branch objects children ->
        case direction (AABB.ofPt pt) box of
          Just (i, subBox) ->
            objects ++ go subBox (children ! i)

          Nothing ->
            error $ "inVicinity: impossible "

      Leaf objects _ ->
        objects

(/%) :: Vector a -> (Int, a -> a) -> Vector a
v /% (i, f) = v // [(i, f (v ! i))]

